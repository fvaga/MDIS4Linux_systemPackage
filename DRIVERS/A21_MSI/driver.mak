#**************************  M a k e f i l e ********************************
#  
#         Author: ts
#  
#    Description: makefile descriptor for A21 MSI enabler helper
#                      
#---------------------------------[ History ]---------------------------------
#
#*****************************************************************************

MAK_NAME=lx_a21_msi_enable

MAK_LIBS=

MAK_SWITCH =

MAK_INCL=

MAK_INP1=a21_msi_enable$(INP_SUFFIX)

MAK_INP=$(MAK_INP1)
